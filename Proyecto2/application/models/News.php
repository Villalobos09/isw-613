<?php

class News extends CI_Model
{

    public function new_registration($title, $description, $link, $pubDate, $id_newssource, $user_id, $category)
    {
        $data = array(
            'title' => $title,
            'description' => $description,
            'link' => $link,
            'pubDate' => $pubDate,
            'id_newssource' => $id_newssource,
            'user_id' => $user_id,
            'category' => $category
        );
        return $this->db->insert('news', $data);
    }

    public function new_filter($title, $description, $link, $pubDate, $id_newssource, $user_id, $category, $filter)
    {
        if ($category === $filter) {
            $data = array(
                'title' => $title,
                'description' => $description,
                'link' => $link,
                'pubDate' => $pubDate,
                'id_newssource' => $id_newssource,
                'user_id' => $user_id,
                'category' => $category
            );
            return $this->db->insert('news', $data);
        }
    }

    public function get_news()
    {
        $user = $_SESSION['users'];
        foreach ($user as $row) {
            $id = $row['id'];
        }
        $this->db->where('user_id', $id);
        $query = $this->db->get('news');
        return $query->result_array();
    }


}
