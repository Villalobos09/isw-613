<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>News Cover</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
 integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link rel="stylesheet" href="Style/login.css">
    <link rel="stylesheet" type="text/css" href="Style/Style.css" media="all">
  </head>
  <body>
  <div class="container">
      <div class="row text-center login-page">
	   <div class="col-md-12 login-form">
        <form action="" method="POST"> 			
	        <div class="row">
		    <div class="col-md-12 login-form-header">
		       <h1><a href="Index.php"><img src="Images/logo.png"></a></h1>
		    </div>
            </div>
        <div class="row">
		   <div class="col-md-12 login-from-row">
		      <input name="usuario" type="email" placeholder="Email" required/>
		   </div>
		</div>
		<div class="row">
		   <div class="col-md-12 login-from-row">
		      <input name="contraseña" type="password" placeholder="Contraseña" required/>
		   </div>
		</div>
        <div class="row">
		   <div class="col-md-12 login-from-row">
               <button type="submit" name="login" value="Ingresar" class="btn btn-secondary"> Login </button><br>
               <a href="UserRegister.php"> Sign Up </a>
		   </div>
		</div>
        </form>
       </div>
     </div>
    </div>
  <?php
  include "Includes/UserLogin.php";
  ?>
  <footer>
    <div class="footer">
      <hr>
      <h4> <a href="">My Cover</a> | <a href="">About</a> | <a href="">Help</a></h4>
      <p>&copy; My News Cover</p>
    </div>
  </footer>

</body>
</html>