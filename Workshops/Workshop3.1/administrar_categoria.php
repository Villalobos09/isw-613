<?php
//incluye la clase Categoria y Crudcateg
require_once('crud_categoria.php');
require_once('categoria.php');
 
$crud= new CrudCategoria();
$categoria= new Categoria();
 
	// si el elemento insertar no viene nulo llama al crud e inserta una categ
	if (isset($_POST['insertar'])) {
		$categoria->setNombre($_POST['nombre']);
		$categoria->setDescripcion($_POST['descripcion']);
		//llama a la función insertar definida en el crud
		$crud->insertar($categoria);
		header('Location: index.php');
	// si el elemento de la vista con nombre actualizar no viene nulo, llama al crud y actualiza la categ
	}elseif(isset($_POST['actualizar'])){
		$categoria->setId($_POST['id']);
		$categoria->setNombre($_POST['nombre']);
		$categoria->setDescripcion($_POST['descripcion']);
		$crud->actualizar($categoria);
		header('Location: index.php');
	// si la variable accion enviada por GET es == 'e' llama al crud y elimina una categ
	}elseif ($_GET['accion']=='e') {
		$crud->eliminar($_GET['id']);
		header('Location: index.php');		
	// si la variable accion enviada por GET es == 'a', envía a la página actualizar.php 
	}elseif($_GET['accion']=='a'){
		header('Location: actualizar.php');
	}
?>