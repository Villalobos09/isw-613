<?php
// incluye la clase Db
require_once('conexion.php');
 
	class CrudCategoria{
		// constructor de la clase
		public function __construct(){}
 
		// método para insertar, recibe como parámetro un objeto de tipo categ
		public function insertar($categoria){
			$db=Db::conectar();
			$insert=$db->prepare('INSERT INTO categorias values(NULL,:nombre,:descripcion)');
			$insert->bindValue('nombre',$categoria->getNombre());
			$insert->bindValue('descripcion',$categoria->getDescripcion());
			$insert->execute();
 
		}
 
		// método para mostrar todos los categ
		public function mostrar(){
			$db=Db::conectar();
			$listaCategorias=[];
			$select=$db->query('SELECT * FROM categorias');
 
			foreach($select->fetchAll() as $categoria){
				$myCategoria= new Categoria();
				$myCategoria->setId($categoria['id']);
				$myCategoria->setNombre($categoria['nombre']);
				$myCategoria->setDescripcion($categoria['descripcion']);
				$listaCategorias[]=$myCategoria;
			}
			return $listaCategorias;
		}
 
		// método para eliminar un categ, recibe como parámetro el id de la categ
		public function eliminar($id){
			$db=Db::conectar();
			$eliminar=$db->prepare('DELETE FROM categorias WHERE ID=:id');
			$eliminar->bindValue('id',$id);
			$eliminar->execute();
		}
 
		// método para buscar una categ, recibe como parámetro el id de la categ
		public function obtenerCategoria($id){
			$db=Db::conectar();
			$select=$db->prepare('SELECT * FROM categorias WHERE ID=:id');
			$select->bindValue('id',$id);
			$select->execute();
			$categoria=$select->fetch();
			$myCategoria= new Categoria();
			$myCategoria->setId($categoria['id']);
			$myCategoria->setNombre($categoria['nombre']);
			$myCategoria->setDescripcion($categoria['descripcion']);
			return $myCategoria;
		}
 
		// método para actualizar una categoria, recibe como parámetro la categoria
		public function actualizar($categoria){
			$db=Db::conectar();
			$actualizar=$db->prepare('UPDATE categorias SET nombre=:nombre, descripcion=:descripcion WHERE ID=:id');
			$actualizar->bindValue('id',$categoria->getId());
			$actualizar->bindValue('nombre',$categoria->getNombre());
			$actualizar->bindValue('descripcion',$categoria->getDescripcion());
			$actualizar->execute();
		}
	}
?>