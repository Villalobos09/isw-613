<?php
//incluye la clase Categoria y CrudCategoria
require_once('crud_categoria.php');
require_once('categoria.php');
$crud=new CrudCategoria();
$categoria= new Categoria();
//obtiene todos las categorias con el método mostrar de la clase crud
$listaCategorias=$crud->mostrar();
?>

<html>
<head>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <div class="container">
        <?php require ('header.php') ?>
        <div class="msg">
            <?php echo $message; ?>
        </div>
    <h1>Mostrar Categorias</h1>
    </div>
</head>
<body>
    <?php require ('header.php') ?>
	<table class="table table-light">
		<tr>
			<th>Nombre</th>
			<th>Descripcion</th>
			<th>Opciones</th>
		</tr>
		<body>
			<?php foreach ($listaCategorias as $categoria) {?>
			<tr>
				<td><?php echo $categoria->getNombre() ?></td>
				<td><?php echo $categoria->getDescripcion() ?></td>
				<td><a href="actualizar.php?id=<?php echo $categoria->getId()?>&accion=a">Actualizar</a> | 
                    <a href="administrar_categoria.php?id=<?php echo $categoria->getId()?>&accion=e">Eliminar</a></td>
			</tr>
			<?php }?>
		</body>
	</table>
	<a href="index.php">Volver</a>
</body>
</html>