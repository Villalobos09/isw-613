<?php
//incluye la clase categ y CrudCategoria
	require_once('crud_categoria.php');
	require_once('categoria.php');
	$crud= new CrudCategoria();
	$categoria=new Categoria();
	//busca el categoria utilizando el id, que es enviado por GET desde la vista mostrar.php
	$categoria=$crud->obtenerCategoria($_GET['id']);
?>
<html>
<head>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <div class="container">
        <?php require ('header.php') ?>
        <div class="msg">
            <?php echo $message; ?>
        </div>
        <h1>Actualice los datos de la categoria</h1>
    </div>
</head>
<body>
	<form action='administrar_categoria.php' method='post'>
	<table>
		<tr>
			<input type='hidden' name='id' value='<?php echo $categoria->getId()?>'>
			<td>Nombre:</td>
			<td> <input type='text' name='nombre' value='<?php echo $categoria->getNombre()?>'></td>
		</tr>
		<tr>
			<td>Descripcion:</td>
			<td><input type='text' name='descripcion' value='<?php echo $categoria->getDescripcion()?>' ></td>
		</tr>
		<input type='hidden' name='actualizar' value'actualizar'>
	</table>
	<input type='submit' value='Guardar'>
	<a href="index.php">Volver</a>
</form>
</body>
</html>