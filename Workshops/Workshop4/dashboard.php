<?php
  session_start();

  $user = $_SESSION['user'];
  if (!$user) {
    header('Location: index.php');
  }
  ?>

  <h1> Bienvenido <?php echo $user['username']?> </h1>
  <a href="logout.php">Logout</a>

  <nav class="nav">
    <?php  if($user['rol'] === 'Administrador') { ?>
      <li class="nav-item">
        <a class="nav-link active" href="../Workshop3.1/index.php">Ver</a>
      </li>
    <?php } ?>
    <?php  if($user['rol'] === 'Estudiante') { ?>
      <li class="nav-item">
        <a class="nav-link active" href="#">Estudiante</a>
      </li>
    <?php } ?>
  </nav>