<html lang="en">
<head>
  <title>User register</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
  <div class="container-fluid">
    <div class="jumbotron">
      <h1 class="display-4">Register</h1>
      <p class="lead">This is the user register process</p>
      <hr class="my-4">
    </div>
    <form action='adminUser.php' method='post'>
      <div class="form-group">
        <label for="username">Username</label>
        <input id="username" class="form-control" type="text" name="username">
      </div>
      <div class="form-group">
        <label for="password">Password</label>
        <input id="password" class="form-control" type="text" name="password">
      </div>
      <div class="form-group">
        <label for="rol">Rol</label>
        <select name="rol">
          <option value="Administrador">Administrador</option>
          <option value="Estudiante">Estudiante</option>
        </select>
      </div>
      <input type='hidden' name='insert' value='insert'>
      <button type="submit" class="btn btn-primary"> Register </button>
      <a href="index.php">Volver</a>
    </form>

  </div>

</body>
</html>