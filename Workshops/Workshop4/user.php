<?php
	class User{
		private $id;
		private $username;
		private $password;
        private $rol;
 
		function __construct(){}
 
		public function getUsername(){
		return $this->username;
		}
 
		public function setUsername($username){
			$this->username = $username;
		}
 
		public function getPassword(){
			return $this->password;
		}
 
		public function setPassword($password){
			$this->password = $password;
		}
        
        public function getRol(){
			return $this->rol;
		}
 
		public function setRol($rol){
			$this->rol = $rol;
		}

		public function getId(){
			return $this->id;
		}
 
		public function setId($id){
			$this->id = $id;
		}
	}
?>